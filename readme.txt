browser-sync start --proxy "production.local/cakephp/cake-tutoriels.local/"
browser-sync start --proxy "cake-tutoriels.local/" --files "src/**/*.php"

bin/cake bake seed --data --fields  id,name,username,email,created,modified Customers
bin/cake bake seed --data --fields id,title,excerpt Articles
bin/cake migrations seed --seed CustomersSeed
bin/cake migrations seed --seed UsersSeed
bin/cake migrations seed --seed PostsSeed
bin/cake migrations seed --seed CategoriesSeed
bin/cake bake seed --data Users
bin/cake bake seed --fields all  Users
bin/cake bake seed --data --fields id,username,email  Users
browser-sync start --proxy "myproject.dev" --files "css/*.css"