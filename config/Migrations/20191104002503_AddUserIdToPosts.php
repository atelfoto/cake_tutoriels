<?php
use Migrations\AbstractMigration;

class AddUserIdToPosts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('posts');
        // $table->addColumn('user_id', 'integer', [
        //     'default' => null,
        //     'limit' => 11,
        //     'null' => true,
        // ]);
        $table->addColumn('category_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('comment_count', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        // $table->addColumn('online', 'boolean', [
        //     'default' => null,
        //     'null' => true,
        // ]);
        $table->update();
    }
}
