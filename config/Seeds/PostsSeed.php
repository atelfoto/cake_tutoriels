<?php
use Migrations\AbstractSeed;

/**
 * Posts seed.
 */
class PostsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        $data = [];
        for ($i=0; $i <50; $i++) {
            $data[] = [
                "name" => $faker->userName,
                "content" => $faker->text,
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                "user_id" => $faker->randomElement($array = [1,2,3,4,5]),
                "category_id" => $faker->randomElement($array = [1,2,3]),
                'online' => $faker->boolean
            ];
        }      

        $table = $this->table('posts');
        $table->insert($data)->save();
    }
}
