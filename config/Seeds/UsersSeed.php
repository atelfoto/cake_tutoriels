<?php

use Migrations\AbstractSeed;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $hasher = new DefaultPasswordHasher();
        $password = $hasher->hash('secret');

        $faker = Faker\Factory::create('fr_FR');
        $data = [];
        for ($i=0; $i <5; $i++) 
        { 
            $data[] = [
                "username" => $faker->userName,
                "email" => $faker->email,
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                "password" => $password,
                'online' => $faker->boolean,
                'group_id' => $faker->randomElement($array = [1,2,3])
            ];
        }

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
