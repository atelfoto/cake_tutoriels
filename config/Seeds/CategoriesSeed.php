<?php
use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        $data = [];
        for ($i=0; $i < 3; $i++)
        {
            $data[] = [
                "name" => $faker->word,
                "content" => $faker->sentence(6),
                "created" => date("Y-m-d H:i:s"),
                "modified" => date("Y-m-d H:i:s")
            ];
        }

        $table = $this->table('categories');
        $table->insert($data)->save();
    }
}
