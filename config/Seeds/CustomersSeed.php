<?php
use Migrations\AbstractSeed;
/**
 * Customers seed.
 */
class CustomersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        $data = [];
        for ($i=0; $i < 10; $i++) 
        { 
            $data[] = [
                "name" => $faker->name(),
                "username" => $faker->userName,
                "email" => $faker->email,
                "created" => date("Y-m-d H:i:s"),
                "modified" => date("Y-m-d H:i:s")
            ];
        }

        $table = $this->table('customers');
        $table->insert($data)->save();
    }
}
