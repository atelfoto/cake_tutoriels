<?php
/**
 * @var \App\View\AppView
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?php $this->append('script'); ?>
<script>
$(document).ready(function() {
	$(document).on('click', '#pagination-container a', function () {
		var thisHref = $(this).attr('href');
		if (!thisHref) {
			return false;
		}
		$('#pagination-container').fadeTo(300, 0);

		$('#pagination-container').load(thisHref, function() {
			$(this).fadeTo(200, 1);
		});
		return false;
	});
});
</script>
<?php $this->end(); ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions'); ?></li>
        <li><?= $this->Html->link(__('New Post'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']); ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']); ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('List Comments'), ['controller' => 'Comments', 'action' => 'index']); ?></li>
        <li><?= $this->Html->link(__('New Comment'), ['controller' => 'Comments', 'action' => 'add']); ?></li>
    </ul>
</nav>
<div class="posts index large-9 medium-8 columns content">
    <h3><?= __('Posts'); ?></h3>
    
   
    <div id="pagination-container">
<?php echo $this->element('../Posts/pagination_container'); ?>
</div>
</div>
